# Global-Game-Jam-2019

This repository is a stub before the start of the [Global Game Jam 2019](https://www.unrealengine.com/en-US/blog/get-ready-for-the-2019-global-game-jam) world event. Our team is actively preparing to participate. And we will definitely make a cool game!

## Development environment

* Unreal Engine 4.21;
    * using [GameJamKit by Tom Shannon](http://www.tomshannon3d.com/2018/01/game-jam-toolkit.html);